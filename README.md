###Webpage that allows you to search for information about a set of Facebook entities (i.e., users, pages, events, places, and groups) using the Facebook Graph API, and the results will be displayed in a tabular format.

Objectives: 

1. Get experience with the PHP programming language.

2. Get experience with the Facebook Graph API.

3. Get experience with the Google Geocoding API.

4. Get experience with the JSON parsers in PHP.

5. Become familiar with the AJAX and JSON technologies.

6. Use a combination of HTML5, Bootstrap, JQuery, Angular, and PHP/Node.js. 

7. Get hands-on experience in Google Cloud App Engine and Amazon Web Services.

8. Get hands-on experience on how to use Bootstrap to enhance the user experience.

http://homework8-env.kamd3vgqsa.us-west-2.elasticbeanstalk.com/